<?php
/**
 * @file
 * Contains Drupal\set_example\Controller\SetExampleController
 */


namespace Drupal\set_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\set\Element\EmptySetElement;
use Drupal\set\Element\FiniteSetElement;
use Symfony\Component\HttpFoundation\Response;


/**
 *
 */
class SetExampleController extends ControllerBase {
  public function example() {
    $set = new FiniteSetElement([
      EmptySetElement::get(),
      new FiniteSetElement([EmptySetElement::get()])
    ]);
    $renderable = $set->toRenderable();
    $response = Response::create(render($renderable));
    $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');
    return $response;
  }
}