<?php
/**
 * @file
 * Contains Drupal\set\IteratorWrapper
 */


namespace Drupal\set;


/**
 *
 */
class IteratorWrapper implements \Iterator {

  /**
   * @var \Iterator
   */
  private $internal;

  /**
   * IteratorWrapper constructor.
   */
  public function __construct(\Iterator $internal) {
    $this->internal = $internal;
  }

  /**
   * The internal iterator is used for the iteration functionality of this iterator.
   * @return \Iterator
   */
  protected function getInternal() {
    return $this->internal;
  }

  /**
   * @inheritDoc
   */
  public function current() {
    return $this->internal->current();
  }

  /**
   * @inheritDoc
   */
  public function next() {
    $this->internal->next();
  }

  /**
   * @inheritDoc
   */
  public function key() {
    return $this->internal->key();
  }

  /**
   * @inheritDoc
   */
  public function valid() {
    return $this->internal->valid();
  }

  /**
   * @inheritDoc
   */
  public function rewind() {
    $this->internal->rewind();
  }
}