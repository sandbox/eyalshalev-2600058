<?php
/**
 * @file
 * Contains Drupal\set\SetIteratorInterface
 */


namespace Drupal\set;


/**
 *
 */
interface SetIteratorInterface extends \Iterator {

  /**
   * Is it iterating over a finite set?
   * @return bool
   */
  public function isFinite();

  /**
   * @inheritdoc
   * @return \Drupal\set\SetInterface
   */
  public function current();
}