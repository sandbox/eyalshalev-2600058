<?php
/**
 * @file
 * Contains \Drupal\set\SetInterface
 */

namespace Drupal\set;

interface SetInterface extends \Serializable {

  /**
   * @return string
   *  The identification string of this set
   */
  public function getId();
}