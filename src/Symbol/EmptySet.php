<?php
/**
 * @file
 * Contains Drupal\set\Finite\EmptySet
 */


namespace Drupal\set\Symbol;

/**
 * The Empty Set
 */
class EmptySet extends FiniteSetSymbolBase {

  /**
   * Constructs a new Empty Set Symbol.
   */
  protected function __construct() {
    parent::__construct([]);
  }
}