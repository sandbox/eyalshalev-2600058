<?php
/**
 * @file
 * Contains Drupal\set\Symbol\SymbolBase
 */


namespace Drupal\set\Symbol;

use Drupal\set\SetBase;
use Drupal\set\Singleton\SingletonTrait;

/**
 *
 */
abstract class SymbolBase extends SetBase implements SymbolInterface {
  use SingletonTrait;

  /**
   * @var mixed
   *    The inner value of this Symbol
   */
  protected $value;

  /**
   * @param mixed $value
   */
  protected function __construct($value) {
    $this->value = $value;
  }

  /**
   * @inheritdoc
   */
  public function getValue() {
    return $this->value;
  }
}