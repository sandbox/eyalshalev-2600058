<?php
/**
 * @file
 * Contains Drupal\set\Finite\EmptySet
 */


namespace Drupal\set\Symbol;

use Drupal\set\Finite\FiniteSet;
use Drupal\set\Finite\FiniteSetInterface;

/**
 * A symbol that represents a finite set.
 */
abstract class FiniteSetSymbolBase extends SymbolBase implements FiniteSetInterface {

  /**
   * Constructs a new Finite Set Symbol.
   * @param \Drupal\set\SetInterface[] $items
   */
  protected function __construct(array $items) {
    parent::__construct(new FiniteSet($items));
  }

  /**
   * @return \Drupal\set\Finite\FiniteSetIterator
   */
  public function getIterator() {
    return $this->getValue()->getIterator();
  }

  /**
   * @return \Drupal\set\Finite\FiniteSetInterface
   */
  public function getValue() {
    return parent::getValue();
  }

  /**
   * @inheritdoc
   */
  public function getId() {
    return $this->getValue()->getId();
  }

  /**
   * @inheritdoc
   */
  public function getItem($itemId) {
    return $this->getValue()->getItem($itemId);
  }

  /**
   * @inheritdoc
   */
  public function serializeData() {
    $data = parent::serializeData();
    $data['value'] = serialize($this->getValue());
    return $data;
  }

  /**
   * @inheritdoc
   * @param string[] $data
   */
  public function unserializeData(array $data) {
    parent::unserializeData($data);
    $this->value = unserialize($data['value']);
  }
}