<?php
/**
 * @file
 * Contains Drupal\set\Symbol\SymbolInterface
 */


namespace Drupal\set\Symbol;

use Drupal\set\Singleton\SingletonInterface;

/**
 *
 */
interface SymbolInterface extends SingletonInterface {

  /**
   * Returns the inner value of this symbol
   * @return mixed
   */
  public function getValue();
}