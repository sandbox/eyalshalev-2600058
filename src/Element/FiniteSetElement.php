<?php
/**
 * @file
 * Contains Drupal\set\Element\FiniteSetElement
 */


namespace Drupal\set\Element;

use Drupal\set\Finite\FiniteSet;

/**
 */
class FiniteSetElement extends FiniteSet implements SetElementInterface {
  use RenderableTraversableSetTrait;

  /**
   * @inheritdoc
   * @param \Drupal\set\Element\SetElementInterface[] $items
   */
  public function __construct(array $items) {
    parent::__construct($items);
  }

  /**
   * @inheritdoc
   * @return \Drupal\set\Element\SetElementInterface
   */
  public function getItem($itemId) {
    parent::getItem($itemId);
  }

  /**
   * @return \Drupal\set\Element\SetElementIteratorInterface
   */
  public function getIterator() {
    return new SetElementIteratorInterface(parent::getIterator());
  }
}