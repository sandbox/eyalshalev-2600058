<?php
/**
 * @file
 * Contains Drupal\set\Element\SetElementInterface
 */


namespace Drupal\set\Element;

use Drupal\Core\Render\RenderableInterface;
use Drupal\set\SetInterface;

/**
 *
 */
interface SetElementInterface extends RenderableInterface, SetInterface {

}