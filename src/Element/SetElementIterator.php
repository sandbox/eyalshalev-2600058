<?php
/**
 * @file
 * Contains \Drupal\set\SetIteratorBase
 */


namespace Drupal\set\Element;

use Drupal\set\Finite\FiniteSetIterator;
use Drupal\set\SetIteratorInterface;

/**
 *
 */
class SetElementIteratorInterface implements SetIteratorInterface {

  /**
   * @var \Drupal\set\SetIteratorInterface
   */
  private $iterator;

  /**
   * @param \Drupal\set\SetIteratorInterface $iterator
   */
  public function __construct(SetIteratorInterface $iterator) {
    $this->iterator = $iterator;
  }

  /**
   * @inheritdoc
   */
  public function isFinite() {
    return $this->iterator instanceof FiniteSetIterator;
  }

  /**
   * @inheritdoc
   */
  public function next() {
    $this->iterator->next();
  }

  /**
   * @inheritdoc
   * @return null||mixed[]
   */
  public function current() {
    /** @var \Drupal\set\Element\SetElementInterface $set */
    if ($this->valid()) {
      $set = $this->iterator->current();
      return $set->toRenderable();
    }
    return FALSE;
  }

  /**
   * @inheritdoc
   */
  public function valid() {
    return $this->iterator->valid();
  }

  /**
   * @inheritdoc
   */
  public function key() {
    return $this->iterator->key();
  }

  /**
   * @inheritdoc
   */
  public function rewind() {
    $this->iterator->rewind();
  }
}