<?php
/**
 * @file
 * Contains Drupal\set\Element\RenderableTraversableSetTrait
 */


namespace Drupal\set\Element;

use Drupal\Core\Template\Attribute;
use Drupal\set\TraversableSetInterface;

/**
 *
 */
trait RenderableTraversableSetTrait {
  /**
   * @inheritdoc
   */
  public function toRenderable() {
    /** @var TraversableSetInterface $this */
    return [
      '#theme' => 'math_set',
      '#items_iterator' => $this->getIterator(),
      '#attributes' => new Attribute(['data-id' => $this->getId()])
    ];
  }
}