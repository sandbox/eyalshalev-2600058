<?php
/**
 * @file
 * Contains Drupal\set\Element\EmptySetElement
 */


namespace Drupal\set\Element;

use Drupal\Core\Render\RenderableInterface;
use Drupal\set\Symbol\EmptySet;

/**
 */
class EmptySetElement extends EmptySet implements RenderableInterface {
  use RenderableTraversableSetTrait;

  /**
   * @inheritdoc
   * @return EmptySetElement
   */
  public static function get() {
    return parent::get();
  }
}