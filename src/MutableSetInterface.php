<?php
/**
 * @file
 * Contains \Drupal\set\MutableSetInterface
 */


namespace Drupal\set;


/**
 *
 */
interface MutableSetInterface extends TraversableSetInterface {

  /**
   * Adds an item to the set
   * @param SetInterface $item
   */
  public function add(SetInterface $item);

  /**
   * Removes an item from the set.
   * @param SetInterface $item
   */
  public function remove(SetInterface $item);
}