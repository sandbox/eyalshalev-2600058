<?php
/**
 * @file
 * Contains Drupal\set\Singleton\SingletonInterface
 */


namespace Drupal\set\Singleton;

/**
 *
 */
interface SingletonInterface {

  /**
   * Returns the single instance of this singleton.
   * @return SingletonInterface
   */
  public static function get();
}