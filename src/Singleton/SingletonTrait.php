<?php
/**
 * @file
 * Contains Drupal\set\Singleton\SingletonTrait
 */


namespace Drupal\set\Singleton;


/**
 *
 */
trait SingletonTrait {

  /**
   * @var SingletonInterface
   * The singleton item.
   */
  private static $singleton = null;

  /**
   * There is only one instance of the singleton
   */
  protected function __construct() {}

  /**
   * Returns the singleton instance
   * @return SingletonInterface
   */
  public static function get() {
    self::$singleton = self::$singleton ? self::$singleton : new static();
    return self::$singleton;
  }
}