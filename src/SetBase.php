<?php
/**
 * @file
 * Contains \Drupal\set\SetBase
 */

namespace Drupal\set;

abstract class SetBase implements SetInterface {

  /**
   * @var
   * A 16 byte integer represented as a hex string formatted with 4 hyphens.
   */
  private $id = NULL;

  /**
   * @inheritdoc
   */
  public function getId() {
    if (!$this->id) {
      $this->setId(\Drupal::service('uuid')->generate());
    }

    return $this->id;
  }

  /**
   * @param string $value
   */
  private function setId($value) {
    $this->id = $value;
  }

  /**
   * @inheritdoc
   */
  public function serialize() {
    return serialize($this->serializeData());
  }

  /**
   * @inheritdoc
   */
  public function unserialize($serialized) {
    $this->unserializeData(unserialize($serialized));
  }

  /**
   * @return string[]
   */
  public function serializeData() {
    $data['id'] = $this->getId();
    return $data;
  }

  /**
   * @param string[] $data
   */
  public function unserializeData(array $data) {
    $this->setId($data['id']);
  }
}