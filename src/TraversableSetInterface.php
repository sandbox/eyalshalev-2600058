<?php
/**
 * @file
 * Contains Drupal\set\TraversableSetBase
 */


namespace Drupal\set;


/**
 *
 */
interface TraversableSetInterface extends SetInterface, \IteratorAggregate {

  /**
   * @return \Drupal\set\SetIteratorInterface
   */
  public function getIterator();
}