<?php
/**
 * @file
 * Contains Drupal\set\Relations\RelationSetInterface
 */


namespace Drupal\set\Relations;

use Drupal\set\Symbol\SymbolInterface;
use Drupal\set\TraversableSetInterface;

/**
 * A relation set has a symbol and set of items.
 */
interface RelationSetInterface extends TraversableSetInterface {

  /**
   * The symbol that represents this relation set.
   * @return SymbolInterface
   */
  public function getSymbol();


  /**
   * Do the relation items conform to this relation?
   * @return bool
   */
  public function testItems();

}