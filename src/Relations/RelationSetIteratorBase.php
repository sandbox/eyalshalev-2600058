<?php
/**
 * @file
 * Contains Drupal\set\Relations\RelationSetIteratorBase
 */


namespace Drupal\set\Relations;

use Drupal\set\IteratorWrapper;

/**
 *
 */
abstract class RelationSetIteratorBase extends IteratorWrapper implements RelationSetIteratorInterface {

  /**
   * The current test result.
   * @var bool
   */
  protected $testResult = true;

  /**
   * @inheritdoc
   */
  public function isFinite() {
    return true;
  }

  /**
   * The current test result.
   * Will be updated with each next call
   * @see RelationSetIteratorBase::next()
   * @return bool
   */
  public function current() {
    return $this->testResult;
  }

  /**
   * @inheritdoc
   */
  public function fold() {
    while ($this->valid()) {
      $this->next();
    }
    return $this->current();
  }
}