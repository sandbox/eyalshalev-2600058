<?php
/**
 * @file
 * Contains Drupal\set\Relations\SetContainsIteratorInterface
 */


namespace Drupal\set\Relations;

use Drupal\set\Finite\FiniteSetIteratorInterface;

/**
 *
 */
interface SetContainsIteratorInterface extends FiniteSetIteratorInterface, RelationSetIteratorInterface {

  /**
   * @inheritdoc
   * @return SetContainsInterface
   */
  public function current();
}