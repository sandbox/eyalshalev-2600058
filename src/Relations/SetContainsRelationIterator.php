<?php
/**
 * @file
 * Contains Drupal\set\Relations\SetContainsRelationIterator
 */

namespace Drupal\set\Relations;

/**
 *
 */
class SetContainsRelationIterator extends RelationSetIteratorBase {

  /**
   * Tests the next item in the relation set.
   * The result can be obtained via the current method
   * @see RelationSetIteratorBase::current()
   */
  public function next() {
    if ($this->testResult) {
      $child = $this->getInternal()->current();
      $this->getInternal()->next();
      $parent = $this->getInternal()->current();

      $this->testResult = $parent->contains($child);
    }
  }

  /**
   * @return \Drupal\set\Relations\SetContainsIteratorInterface
   */
  protected function getInternal() {
    return parent::getInternal();
  }
}