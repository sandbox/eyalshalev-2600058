<?php
/**
 * @file
 * Contains Drupal\set\Relations\SetContainsInterface
 */

namespace Drupal\set\Relations;

use Drupal\set\SetInterface;

interface SetContainsInterface extends SetInterface {

  /**
   * Checks if this set contains ALL of the $children
   *
   * @param \Drupal\set\SetInterface ...$children
   * @return bool
   */
  public function contains(SetInterface ...$children);
}