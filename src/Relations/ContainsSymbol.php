<?php
/**
 * @file
 * Contains Drupal\set\Relations\ContainsSymbol
 */


namespace Drupal\set\Relations;
use Drupal\set\Symbol\SymbolBase;


/**
 *
 */
class ContainsSymbol extends SymbolBase {

  /**
   * Creates the single contains symbol
   */
  protected function __construct() {
    parent::__construct('∋');
  }
}