<?php
/**
 * @file
 * Contains Drupal\set\Relations\RelationSetIteratorInterface
 */


namespace Drupal\set\Relations;

use Drupal\set\SetIteratorInterface;

interface RelationSetIteratorInterface extends SetIteratorInterface {
  /**
   * Test all the items in the iterator and return the result.
   * @return bool
   */
  public function fold();
}