<?php
/**
 * @file
 * Contains Drupal\set\Relations\RelationSetBase
 */


namespace Drupal\set\Relations;

use Drupal\set\SetBase;

/**
 * An abstract base class for relations.
 */
abstract class RelationSetBase extends SetBase implements RelationSetInterface {

  /**
   * @var \Drupal\set\SetIteratorInterface
   */
  protected $items;

  /**
   * @param \Drupal\set\Relations\RelationSetIteratorInterface $items
   */
  public function __construct(RelationSetIteratorInterface $items) {
    $this->items = $items;
  }

  /**
   * @inheritdoc
   *
   * The internal iterator is used to test the relation items.
   * @return \Drupal\set\Relations\RelationSetIteratorInterface;
   */
  public function getIterator() {
    $this->items;
  }
}