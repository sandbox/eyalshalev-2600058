<?php
/**
 * @file
 * Contains Drupal\set\Relations\SetContainsRelation
 */


namespace Drupal\set\Relations;

/**
 *
 */
class SetContainsRelation extends RelationSetBase {

  /**
   * @param \Drupal\set\Relations\SetContainsIteratorInterface $items
   */
  public function __construct(SetContainsIteratorInterface $items) {
    parent::__construct($items);
  }

  /**
   * @return \Drupal\set\Singleton\SingletonInterface
   */
  public function getSymbol() {
    return ContainsSymbol::get();
  }

  /**
   * Do the relation items conform to this relation?
   * @return bool
   */
  public function testItems() {
    return $this->getIterator()->fold();
  }
}