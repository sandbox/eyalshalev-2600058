<?php
/**
 * @file
 * Contains \Drupal\set\Finite\MutableFiniteSet
 */


namespace Drupal\set\Finite;

use Drupal\set\MutableSetInterface;
use Drupal\set\SetInterface;

/**
 *
 */
class MutableFiniteSet extends FiniteSet implements MutableSetInterface {

  /**
   * @inheritdoc
   */
  public function add(SetInterface $item) {
    $this->items[$item->getId()] = $item;
  }

  /**
   * @inheritdoc
   */
  public function remove(SetInterface $item) {
    unset($this->items[$item->getId()]);
  }
}