<?php
/**
 * @file
 * Contains Drupal\set\Finite\FiniteSetIteratorInterface
 */


namespace Drupal\set\Finite;

use Drupal\set\SetIteratorInterface;

/**
 *
 */
interface FiniteSetIteratorInterface extends SetIteratorInterface {

  /**
   * Should return true
   * @return TRUE
   */
  public function isFinite();
}