<?php
/**
 * @file
 * Contains Drupal\set\Finite\FiniteSetInterface
 */


namespace Drupal\set\Finite;

use Drupal\set\Relations\SetContainsInterface;
use Drupal\set\TraversableSetInterface;

/**
 *
 */
interface FiniteSetInterface extends TraversableSetInterface, SetContainsInterface {

  /**
   * Returns the item with the given uuid.
   * Or null if not exist.
   * @param string $itemId
   *    A 16 byte integer represented as a hex string formatted with 4 hyphens.
   * @return null|\Drupal\set\SetInterface $item
   */
  public function getItem($itemId);

}