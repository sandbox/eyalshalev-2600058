<?php
/**
 * @file
 * Contains Drupal\set\Finite\FiniteSetIterator
 */


namespace Drupal\set\Finite;

use Drupal\set\IteratorWrapper;
use Drupal\set\SetInterface;

/**
 *
 */
class FiniteSetIterator extends IteratorWrapper implements FiniteSetIteratorInterface {

  /**
   * @inheritdoc
   */
  public function isFinite() {
    return true;
  }

  /**
   * Creates an iterator for
   * @param SetInterface[] $items
   */
  public function __construct(array $items) {
    parent::__construct(new \ArrayIterator($items));
  }
}