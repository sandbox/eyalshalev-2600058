<?php
/**
 * @file
 * Contains Drupal\set\Finite\FiniteSet
 */

namespace Drupal\set\Finite;

use Drupal\set\SetBase;
use Drupal\set\SetInterface;

/**
 *
 */
class FiniteSet extends SetBase implements FiniteSetInterface {

  /**
   * @var \Drupal\set\SetInterface[]
   */
  protected $items = [];

  /**
   * @param \Drupal\set\SetInterface[] $items
   */
  public function __construct(array $items) {
    foreach ($items as $item) {
      $this->items[$item->getId()] = $item;
    }
  }

  /**
   * @param \Drupal\set\SetInterface ...$children
   * @return bool
   */
  public function contains(SetInterface ...$children) {
    foreach ($children as $child) {
      if (!in_array($child, $this->items)) {
        return false;
      }
    }
    return true;
  }

  /**
   * @inheritdoc
   */
  public function getItem($itemId) {
    return $this->items[$itemId];
  }

  /**
   * @inheritdoc
   * @return string[][]
   */
  public function serializeData() {
    $data = parent::serializeData();
    $data['items'] = [];
    foreach ($this->getIterator() as $id => $set) {
      $data['items'][$id] = serialize($set);
    }
    return $data;
  }

  /**
   * @return \Drupal\set\Finite\FiniteSetIterator
   */
  public function getIterator() {
    return new FiniteSetIterator($this->items);
  }

  /**
   * @inheritdoc
   * @param string[][] $data
   */
  public function unserializeData(array $data) {
    parent::unserializeData($data);

    foreach ($data['items'] as $serializedItem) {
      /** @var \Drupal\set\SetInterface $item */
      $item = unserialize($serializedItem);
      $this->items[$item->getId()] = $item;
    }
  }
}